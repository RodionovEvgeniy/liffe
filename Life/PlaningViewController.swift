//
//  PlaningViewController.swift
//  Life
//
//  Created by Евгений Родионов on 04/05/2019.
//  Copyright © 2019 Евгений Родионов. All rights reserved.
//

import UIKit

class PlaningViewController: UIViewController {
    @IBOutlet weak var categoriesTextField: UITextField!
    
    @IBOutlet weak var taskTextField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    var categories = [(category: Category, isExpanded: Bool)]()     //создаем изменяемое массив состоящее из категорий типа "Категория" и булевой переменной, которая проверяет                                                                  //открыта ячейка или нет
    var currentCategory: Category?                                  //создаем переменную типа "Категория", которая отвечает за текущую категорию
    var tasks = [Task]()                                            //создаем массив задач типа "Задачи"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    
        let nib = UINib(nibName: "CategoryHeaderView", bundle: Bundle.main)            // создаем файл типа nib   со свойством имя, хранилище - системные ресурсы текущего проекта
        tableView.register(nib, forHeaderFooterViewReuseIdentifier: "Category Header") // регистрируем в TV новый объект для переиспользующийся HFV с именем Category Header
        let path = Bundle.main.path(forResource: "Categories", ofType: "json")!        // создаем константу путь", которая использует хранилище текущего проекта, где название искомого                                                                             //обьекта "Categories" типа json
        let url = URL(fileURLWithPath: path)                                           // создаем объект урл типа УРЛ, где путь к файлу вышесозданный "путь"
        do {
             let data = try Data(contentsOf: url)                                       // пробуем получить данные содержащиеся в урл
             let cat = try JSONDecoder().decode([Category].self, from: data)            // пробуем декодировать jsonдекодером данные типа "Category"
            categories = cat.map({ (name: $0, isExpanded: true)})                       //преобразуем массив категорий, где свойство открытости ячейки - истина
        } catch {
            print("111")                                                                 // если не получается, то что пробуем, печатаем 111
        }
        
    

    }
    
    @IBAction func addTask(_ sender: UIButton) {                                         //создаем экшн по кнопке "+" добавить задачу
        guard let taskName = taskTextField.text, let categoryCode = currentCategory?.code else {  // устанавливаем в задачу текст текстового поля и в код категории - код текущей                                                                                             //категории
                return }
        let task = Task(name: taskName, categoryCode: categoryCode, countTask: tasks.count )       // создаем задачу типа "Task", присваиваем имя, код категории и количество задач
        tasks.append(task)                                                                         // добавляем нашу задачу в массив задач типа "Задачи"
        taskTextField.resignFirstResponder()                                                       //после добавления задачи убираем каретку из текстового поля
        DispatchQueue.main.async {                                                                 // добавляем в головной поток асинхронную задачу
                let section = self.categories.lastIndex(where: { task.categoryCode == $0.category.code})! //?? создаем секцию, в которой храним последний текущий элемент множества, где код категории задачи совпадает с кодом выбранной кагетории
                self.tableView.reloadSections([section], with: .automatic)    //перезагрузить текущие секции, установить новой секции индекс созданной секции
        }
        
        
        
    }
    
    
    @IBAction func down(_ sender: UITextField) { //экшн по активному нажатию на текстфилд
        let alert = UIAlertController(title: "Choose category", message: nil, preferredStyle: .alert) // создаем алерт экземпляр класса AC, устанавливаем текст "Choose category", выбираем стиль алерт
       //alert.addTextField()
       // alert.isModalInPopover = true                                                                 //задаем свойтсво "быть над всеми слоями" нашего алерта тру
        let cancel = UIAlertAction(title: "cancel", style: .cancel, handler: nil)                     //создаем экземпляр AA типа поведения "cancel"
        alert.addAction(cancel)                                                                       //добавляем экшн типа поведения "cancel" в наш алерт
        alert.view.translatesAutoresizingMaskIntoConstraints = false                                  //устанавливаем в автоматической настройке констрейнтов алерта значение false
        let picker = UIPickerView(frame: CGRect(x: 0, y: 0, width: alert.view.frame.width, height: 200)) //создаем экземпляр PV, ширина которого равна ширине алерта, высота равна 200
        alert.view.addSubview(picker)                                                                 //добавляем в алерт наш пикер
        picker.translatesAutoresizingMaskIntoConstraints = false                                      // автонастройку констрейнтов пикера делаем равной false
        picker.centerXAnchor.constraint(equalTo: alert.view.centerXAnchor).isActive = true            // устанавливаем координату x пикера равной x алерта
        picker.centerYAnchor.constraint(equalTo: alert.view.centerYAnchor).isActive = true            // устанавливаем координату у пикера равной у алерта
        alert.view.heightAnchor.constraint(equalToConstant: 300).isActive = true                      // устанавливаем высоту алерта 300, чтобы была возмлжность выбрать любую категорию
        picker.dataSource = self                                                                      //устанавливаем ориентир dataSource нашего пикера текущее значения самого себя
        picker.delegate = self                                                                        //устанавливаем ориентир delegate нашего пикера текущее значения самого себя
        let ok = UIAlertAction(title: "ok", style: .default) { (_) in                                 //создаем АА, где возвращаем индекс
            let row = picker.selectedRow(inComponent: 0)                                              // выбранного элемента пикера
            self.currentCategory = self.categories[row].category                                      //текущее значение "текущ. категории" устанавливается по нашему индексу из          множества категорий
            self.categoriesTextField.text = self.currentCategory?.name                                //устанавливаем значение текс поля категорий равным имени текущей категории
        }
        alert.addAction(ok)                                                                           // добавляем наш АА  в алерт
        
       // alert.popoverPresentationController?.sourceView = sender                                      // добавляем объект PC в алерт
      
        
        present(alert, animated: true, completion: nil)                                               // презентуем алерт с анимацей
    }
    
    

}

extension PlaningViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1                                                                                       // разрешаем сделать выбранным только один компонент в пикере?
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return categories.count                                                                        //устанавливаем количество компонентов в пикере равным количеству категорий
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return categories[row].category.name                                                            // устанавливаем названия для компонентов пикера, соответственно имени категорий
    }
    
}

extension PlaningViewController: UITableViewDataSource, UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {                                           // устанавливаем количество секций равным количеству категорий
        return categories.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {               // устанавливаем количество строк в секции
        let category = categories[section]
        return category.isExpanded ? tasks.filter({ $0.categoryCode == category.category.code}).count : 0 // проверяем если булево значение "открытости ячейки" правда, устанавливаем количество строк в секции равной количеству задач, где код категории совпадает с кодом категории в наших категориях, если ложь, то количество строк = 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {             // устанавливаем значения в секциях хэдэра
        if let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "Category Header") as? CategoryHeaderView { // если header может переиспользовать HF  с идентификатором
                                                                              // и апкастит к нашему CHW то
            header.categoryLabel.text = categories[section].category.name     // устанавливаем в лэйбл имя категории по секции
            header.categoryCode = categories[section].category.code           // в код категории устанавливаем код категории по секции
            header.isExpanded = categories[section].isExpanded                // в "открытость ячейки" устанавливаем "открытость ячейки"
            header.tag = section                                              //?? в tag  устанавливаем индекс по секции
            let code = header.categoryCode                                    //
            let filteredTasks = tasks.filter { (task) -> Bool in              //
                return task.categoryCode == code ? true : false               // возвращаем массив задач при совпадении кода категории задачи и кода категории HV
            }
            //let fil = tasks.filter{$0.categoryCode == code}
            
            header.simpleButton.setTitle(String(filteredTasks.count), for: .normal)   //устанавливаем количество задач в title кнопки
            header.delegate = self                                                    // устанавливаем в значение делегат текущий хэдэр
            return header
        }
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Task Cell", for: indexPath) as! CustomTableViewCell // переиспользуем ячейки "Task Cell"  по IndexPath апкастим к                                                                                                                //кастомной ячейке
        let section = indexPath.section
        if let header = tableView.headerView(forSection: section) as? CategoryHeaderView {       //проверяем если экз HW для секций по секциям indexPath апкастится к CHW
            let categoryCode = header.categoryCode                                               // передаем код категории хэдэра
            let tas = tasks.filter{$0.categoryCode == categoryCode}                              // фильтруем задачи, где переданный код категориии равен коду категории задачи
            cell.taskName.text = tas[indexPath.row].name                                         // присваиваем имя задачи имени задачи по строке по indexPath
            cell.checkBtn.isChecked = tas[indexPath.row].isDone                                  //?? устанавливаем булевое значение "была выбрана" из "isDone"
            
//            let tc = String(tas.count)
//            let ssa = CategoryHeaderView()
//            if tc != nil {
//                ssa.simpleButton?.setTitle(tc, for: .normal)
//            }
        }
//        let checkBox = CheckBox() // выделение строки зеленым цветом
//        if checkBox.isChecked == false{
//            let cell = tableView.cellForRow(at: indexPath)
//            cell?.backgroundColor = .green
//        }
        
        
       // cell.textLabel?.text = categories[indexPath.row].name
    
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true                                                                            // разрешаем редактирование строк по indexPath
    }
    

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete{                                                            // задаем стиль редактирования - удаление
            let categoryHeader = tableView.headerView(forSection: indexPath.section) as! CategoryHeaderView //создаем экз HW для секций по секциям indexPath где HW -> CHW
            let _tasks = tasks.filter{$0.categoryCode == categoryHeader.categoryCode}                       // фильтруем задачи по коду категории
            let taskToRemove = _tasks[indexPath.row]                                                        // создаем задачи для удаления из отфильтрованных задач
            
            tasks.removeAll { (task: Task) -> Bool in                                                       //удаляем задачи из Task
                return taskToRemove === task                                                                // где задачи равны задачам для удаления
            }
            
            DispatchQueue.main.async {                                                                      //добавляем в главный поток асинхронную задачу
                self.tableView.reloadSections(IndexSet(arrayLiteral: indexPath.section), with: .automatic)  // перезагрузить секции, установить индекс по indexPath секций
            }
        }
    }
    
    
}

//
extension PlaningViewController: CategoryHeaderViewDelegate{
    func simpleDidTap(header: CategoryHeaderView) {                                          // реализуем функцию содержащую header по нажатию кнопки с количеством задач
        let section = header.tag                                                             //?? установим в секцию значение tag по хэдэру
        categories[section].isExpanded = !categories[section].isExpanded                     // изменяем значение "открытости секции" на противоположное
        DispatchQueue.main.async {                                                           // добавляем в главный поток асинхр. задачу
            self.tableView.reloadSections([section], with: .fade) // индекс секции по коду   // релоадим секции в текущем TW
        }
        
    }


}
